# Node.js Authentication Starter using REST

This is a great starting point for a new project which requires authentication or for anyone interested in the basics of authentication using REST principles.


### Prerequisites

This project assumes you have Node installed on the machine which you are planning to run the application on. The project also uses MongoDB with the Mongoose ODM (object document mapper).
Although this project uses MongoDB, you are free to use any database but will have to alter the code accordingly.

### Installing

Clone the project into your desired directory

```
git clone https://gitlab.com/Annarino/authentication-rest.git
```

Once cloned, run the following command to install all of the dependencies

```
 npm install
```

To run the development environment, ensure you add a nodemon.js file to the root with the following environment variables

```
{
    "env":{
        "MONGO_URI":"the URI to your MongoDB connection",
        "JWT_SECRET_KEY":"used to hash the JSON web token",
        "PORT": 8080
    }
}
```

To start the development server:
```
npm run start:dev
```

To run the production server

```
npm start
```

## End Points

To register a new user:
```
/users/signup
Method: POST
Body:{
        "firstName": "User's first name",
        "surname": "User's surname",
        "email": "User's email",
        "password": "User's password"
    }
Response: 
    201 - If the user was successfully added to the database.
    409 - If there already exists a user with that email.
    500 - General server error
```

To login a user:
```
/users/login
Method: POST
Body:{
        "email": "User's email",
        "password": "User's password"
    }
Response:
    200 + {"token":"JSON web token"} - If the user submits the correct credentials.
    401 - If the email or the password does not match.
```

To edit a user:
```
/users
Method: PUT
Body:{
        "firstName": "User's first name",
        "surname": "User's surname",
        "email": "User's email"
    }
Response:
    200 + {"token":"JSON web token", "user": "updated user information"} - If user was sucessfully updated
    401 - If the user is not authorised or if the email in the token does not match one on file

You must ensure you update the token on the frontend application otherwise the user will not be have permission to use any of the protected routes.

```

To edit a user's password:
```
/users/edit-password
Method: PUT
Body:{
        "password":"updated password"
    }
Return: 
    200 - If the password was successfully updated
    401 - If the user does not have permission to access this.
    400 - if there was a general error.
```

## Running the tests

Tests are in the making, check back soon...



## Built With

* [Express.js](https://expressjs.com/) - Easy to use framework for Node.js
* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) - Package for creating and reading the JSON web tokens.
* [bcrypt](https://www.npmjs.com/package/bcrypt) - Used to securely store passwords.
* [mongoose](https://mongoosejs.com/) - Object document mapper for MongoDB documents.
* [nodemon](https://nodemon.io/) - Automatically restarts server when developing.


## Author

* **Anthony Annarino** - *Initial work* - [Annarino](https://gitlab.com/annarino)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Maximilian Schwarzmüller - Checkout his complete [Node.js course](https://www.udemy.com/course/nodejs-the-complete-guide/) on Udemy 
