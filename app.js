const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const mongoose = require('mongoose');

//Routing Middleware
const userRoutes = require('./routes/user');

const app = express();

app.use(bodyParser.json());

//Add additional routes for your application here
app.use('/users', userRoutes);

const server = http.createServer(app);
mongoose.connect(process.env.MONGO_URI, {useNewUrlParser:true, useUnifiedTopology: true}).then(result=>{
    console.log('connected');
    app.listen(process.env.PORT || 8080);
});
