const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const User = require('../models/user.js');

/**
 * Checks if a user already exists based on email. 
 * @param {String} email
 * @returns {(boolean|User)} User if they exist else false
 */
async function checkUserExistence(email){
    try{
    const user = await User.findOne({email});
    if(user){
        return user;
    }
    return false
    } catch(err){
        next(err);
    }
}

exports.addUser = async (req, res, next) => {
    try{
        const userExists = await checkUserExistence(req.body.email);
        if(!userExists){
            const password = await bcrypt.hash(req.body.password, 12);
            const newUser =  new User({
                firstName: req.body.firstName,
                surname: req.body.surname,
                email: req.body.email,
                password
            })
            const addedUser = await newUser.save(newUser);
            return res.status(201).json(addedUser);
        }
        return res.status(409).end();
    }
    catch(err){
        next(err);
    }
}

exports.editUser = async (req, res, next) => {
    let updatedUser = {
        firstName : req.body.firstName,
        surname : req.body.surname
    }
    let updatedEmail = req.body.email.toString().toLowerCase().trim();
    let currentEmail = req.email.toString().toLowerCase().trim();
    if(updatedEmail === undefined || updatedEmail === null || updatedEmail === "" || updatedEmail === currentEmail){
        updatedEmail = false;
    } 
    try {
        if(updatedEmail){
            const emailExists = await checkUserExistence(updatedEmail);
            if(emailExists){
                return res.status(409).end();
            }
            updatedUser = {...updatedUser, email : updatedEmail};
        }
        console.log(updatedUser)
        const user = await User.findOneAndUpdate({email:currentEmail}, updatedUser, {useFindAndModify:false, new: true});
        if(!user){
            return res.status(401).end();
        }
        const token = jwt.sign({email:user.email}, process.env.JWT_SECRET_KEY)
        return res.status(200).json({user, token});
    } catch (error) {
        console.log(error)
    }
}

exports.editPassword = async(req, res, next) => {
    try{
        const newPassword = await bcrypt.hash(req.body.password, 12);
        const user = await User.findOneAndUpdate({email:req.email},{password:newPassword}, {useFindAndModify: false});
        if(!user){
            return res.status(400).end()
        }
        return res.status(200).end();
    } catch(error){
        console.log(error)
    }
    
}

exports.userLogin = async (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    try {
        const user = await User.findOne({email});
        if(!user){
            return res.status(401).end();
        }
        const checkPw = await bcrypt.compare(password, user.password);
        if(!checkPw){
            return res.status(401).end();
        }
        //You can add anything to the Web Token here. By default this application adds the user email.
        const token = jwt.sign({email}, process.env.JWT_SECRET_KEY);
        return res.status(200).json({token});
    } catch (error) {
        console.log('error',error)
        return res.status(401).end();
    }
}
