const jwt = require('jsonwebtoken');
const User = require('../models/user');

exports.authorize = async (req, res, next) => {
    let decodedToken;
    //Check for an authorization header
    if(!req.get('Authorization')){
        return res.status(401).json({"message":"No authorization header."});
    }
    try{
        const token = req.get('Authorization').split(' ')[1];
        decodedToken = jwt.verify(token, process.env.JWT_SECRET_KEY)
    } catch(err){
        err.statusCode = 401;
        res.status(err.statusCode).send()
        throw err;
    }
    if(!decodedToken || Object.keys(decodedToken).length === 0){
        const err = new Error();
        err.statusCode = 401;
        res.status(err.statusCode).send();
        return err;
    }
    //Checks if the user exists in the database
    req.email = decodedToken.email;
    const user = await User.findOne({email: decodedToken.email})
    if(!user){
        return res.status(401).end;
    }
    return next();
}