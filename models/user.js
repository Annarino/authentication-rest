const mongoose = require('mongoose');

const User =  new mongoose.Schema({
    firstName:{
        type: String,
        required: true
    },
    surname:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    username: String,
    password: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('User', User);
