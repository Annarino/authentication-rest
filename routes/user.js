const express = require('express');
const user = require('../controllers/user');
const auth = require('../middleware/authentication');

const router = express.Router();


router.post('/signup', user.addUser);

//Returns a JSON web token if the email and password combination matches one in the database
router.post('/login', user.userLogin);

//Returns a status code and a new JSON web token if update successful. You must replace the token on the frontend application with the new one.
router.put('/', auth.authorize, user.editUser);
router.put('/edit-password', auth.authorize, user.editPassword)

module.exports = router;